# Fake Metrics Service

The Fake Metrics Service is a simple Go microservice designed to generate and return fake metrics data in JSON format. It's ideal for testing monitoring tools, data visualization dashboards, and any application that consumes metrics data. The service simulates metrics such as CPU usage, memory usage, disk I/O, and network I/O, providing a convenient way to generate data for development and testing purposes without the need for actual metrics sources.

## Getting Started

### Prerequisites

- Go (version 1.20 or later recommended)

### Installation

1. **Clone the repository:**

   ```bash
   git clone https://github.com/yourgithubusername/fake-metrics-service.git

2. **Navigate to the project directory:**

    ```bash
    cd fake-metrics-service
    ```
3. **There's no need to manually install dependencies.** Go Modules will automatically handle them when you build or run the service.

### Running the Service
Execute the following command in the terminal at the project's root directory to start the service:

```bash
go run main.go
```
The service will start on port 8080. Ensure that the port is available or adjust the port as necessary in the source code.

### Usage
With the service running, you can fetch the fake metrics by making a GET request to the /metrics endpoint:

```bash
http://localhost:8080/metrics
```
You can use tools like curl, Postman, or simply your web browser to make the request.

### Example using curl:

```bash
curl http://localhost:8080/metrics
```

### Response example:

```json
[
  {"name": "cpu_usage", "value": 57.81},
  {"name": "memory_usage", "value": 68.42},
  {"name": "disk_io", "value": 1024.55},
  {"name": "network_io", "value": 256.39}
]
```
The response includes a JSON array with objects representing each metric, including a name and value.

### Features
Simple REST API: A single endpoint to generate and retrieve a set of fake metrics.
Random Metrics Generation: Simulates various metrics with random values to represent system and network behavior.
JSON Response: Metrics are returned in JSON format for easy consumption by clients.