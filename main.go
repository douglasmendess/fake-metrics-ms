package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
)

type Metric struct {
	Name  string  `json:"name"`
	Value float64 `json:"value"`
}

func generateFakeMetrics() []Metric {
	return []Metric{
		{"cpu_usage", rand.Float64() * 100},
		{"memory_usage", rand.Float64() * 100},
		{"disk_io", rand.Float64() * 1000},
		{"network_io", rand.Float64() * 100},
	}
}

func handleMetricsRequest(w http.ResponseWriter, r *http.Request) {
	metrics := generateFakeMetrics()

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(metrics)
}

func main() {
	http.HandleFunc("/metrics", handleMetricsRequest)
	fmt.Println("Fake metrics Service started on port 8080")
	http.ListenAndServe(":8080", nil)
}
