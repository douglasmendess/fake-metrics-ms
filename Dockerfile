FROM golang:1.21.4 as builder

WORKDIR /app

COPY go.* ./
RUN go mod download

COPY . ./

RUN CGO_ENABLED=0 GOOS=linux go build -v -o fake-metrics-service 2>&1 | tee build.log

RUN cat build.log

FROM scratch AS runtime

COPY --from=builder app/fake-metrics-service /fake-metrics-service

CMD ["/fake-metrics-service"]
